# Monorail Issue Tracker Archive

This is a read-only archive of the Monorail Issue Tracker. Monorail was
Chromium's Issue Tracker during 2016-2024. Please use this archive if you wish
to fork Monorail for your own use or to reference historical code.

## Using this archive

This archive is a copy of Chromium's entire
[infra/infra](https://chromium.googlesource.com/infra/infra) repository.
Monorail's code is located at [appengine/monorail/](appengine/monorail/), and
instructions for its use are under that directory's
[README.md](appengine/monorail/README.md).

Monorail depends on other repositories using git submodules. Use `git submodule
init` and `git submodule update` to include them.

## Archive status

This project is no longer maintained by the Chromium authors and not accepting
contributions. No support will be provided. Monorail may have known issues or
vulnerabilities that will not be fixed. The archive of historical Monorail
issues for reference only is in the Google Issue Tracker under component ID
[1644405](http://issuetracker.google.com/issues?q=status:open%20componentid:1644405%2B).

## License

This repository is licensed under a BSD-style license that can be found in the
LICENSE file.
