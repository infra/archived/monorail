// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package controller contains the implementation to interact and manipulate the
// data models of the Device Lease service.
package controller
