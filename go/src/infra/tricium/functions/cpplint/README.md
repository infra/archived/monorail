# Cpplint Parser

Tricium analyzer checking for checking C++ code.

Consumes Tricium FILES and produces Tricium RESULTS comments.

## Development and Testing

Local testing:

```
$ make
$ ./cpplint_parser --input=test --output=out
```
