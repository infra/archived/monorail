# Git File Isolator Analyzer

Consuming Tricium GIT_FILE_DETAILS data and producing a Tricium FILES data.

## Testing

Local testing:

```
$ go build -o isolator
$ ./isolator --input=test --output=out
```
