// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package testscenarios contains only tests.
//
// This is used to test high-level behavior through the RPC interface.
package testscenarios
