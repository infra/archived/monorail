// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"go.chromium.org/chromiumos/config/go/test/api"
	server "go.chromium.org/chromiumos/test/ctpv2/common/server_template"

	"infra/cros/cmd/common_lib/common"
)

const (
	binName                            = "al-provision-filter"
	androidBuildInternalScope          = "https://www.googleapis.com/auth/androidbuild.internal"
	cloudPlatformScope                 = "https://www.googleapis.com/auth/cloud-platform"
	androidBuildInternalBuildsEndpoint = "https://androidbuildinternal.googleapis.com/android/internal/build/v3/builds"
	gceServiceAccountJSONPath          = "/creds/service_accounts/service-account-chromeos.json"
)

// ALProvisionRequestUpdater struct stores
type ALProvisionRequestUpdater struct {
	ProvisionPath string
	ServoPath     string

	LatestBuildsByBoard map[string]int
}

func (pru *ALProvisionRequestUpdater) executor(req *api.InternalTestplan, log *log.Logger) (*api.InternalTestplan, error) {
	log.Println("Executing AL provision Filter - Updates provision request.")
	dockerKeyFile, err := common.LocateFile([]string{common.LabDockerKeyFileLocation, common.VmLabDockerKeyFileLocation})
	if err != nil {
		log.Println(fmt.Errorf("unable to locate dockerKeyFile: %w", err))
	}

	pru.ProvisionPath, err = processContainerPath(context.Background(), dockerKeyFile, pru.ProvisionPath, "foil-provision", log)
	if err != nil {
		return req, err
	}
	pru.ServoPath, err = processContainerPath(context.Background(), dockerKeyFile, pru.ServoPath, "servo-nexus", log)
	if err != nil {
		return req, err
	}

	if err := GenerateDynamicProvisionUpdates(req, pru, log); err != nil {
		log.Printf("Error while generating dynamic updates, %s", err)
		return req, err
	}
	log.Println("Finished generating dyanmic updates.")

	return req, nil

}

func processContainerPath(ctx context.Context, creds, path, firestoreName string, log *log.Logger) (processedPath string, err error) {
	switch path {
	case common.LabelProd, common.LabelStaging:
		testContainer, err := common.FetchFilterFromFirestore(ctx, creds, path, firestoreName)
		if err != nil {
			return "", fmt.Errorf("failed to fetch %s, %w", firestoreName, err)
		}
		processedPath, err = common.CreateImagePath(testContainer.GetContainerInfo().GetContainer())
		if err != nil {
			return "", fmt.Errorf("failed to create image path, %w", err)
		}
	default:
		processedPath = path
	}

	return
}

func main() {
	provisionRequestUpdater := &ALProvisionRequestUpdater{
		LatestBuildsByBoard: make(map[string]int),
	}
	fs := flag.NewFlagSet("Run Al provision filter", flag.ExitOnError)
	fs.StringVar(&provisionRequestUpdater.ProvisionPath, "prov-path", common.LabelProd, "SHA256 value for provision container")
	fs.StringVar(&provisionRequestUpdater.ServoPath, "servo-path", common.LabelProd, "SHA256 value for servo-nexus container")
	err := server.ServerWithFlagSet(fs, provisionRequestUpdater.executor, "request-updater")
	if err != nil {
		os.Exit(2)
	}
	os.Exit(0)
}
