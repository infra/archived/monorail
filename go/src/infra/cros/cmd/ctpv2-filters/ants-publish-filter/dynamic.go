// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.
package main

import (
	"log"
	"strconv"

	"google.golang.org/protobuf/types/known/anypb"

	"go.chromium.org/chromiumos/config/go/test/api"
	"go.chromium.org/chromiumos/config/go/test/api/metadata"
	labapi "go.chromium.org/chromiumos/config/go/test/lab/api"
	"go.chromium.org/chromiumos/test/ctpv2/common/dynamic_updates"
	"go.chromium.org/chromiumos/test/ctpv2/common/dynamic_updates/builders"
	dynamic_common "go.chromium.org/chromiumos/test/ctpv2/common/dynamic_updates/common"
	"go.chromium.org/chromiumos/test/ctpv2/common/dynamic_updates/generators"
)

const (
	artifactsDir      = "/tmp/ants-publish"
	runCmd            = "ants-publish server -port 0"
	containerID       = "ants-publish"
	internalAccountID = 1
	dynamicIdentifier = "ants-publish"
	skipTFUploadFlag  = "skip_ants_upload"
)

func isInternal(accountID string) bool {
	// Sometimes, we do not have accountId for internal users.
	if accountID == "" {
		return true
	}

	id, err := strconv.Atoi(accountID)
	if err != nil {
		log.Printf("Cannot convert account %s to int", accountID)
		return false
	}

	if id < 1 {
		log.Printf("Account ID %s not supported", accountID)
		return false
	}

	return id == internalAccountID
}

func skipAntsPublish(metadata *metadata.PublishAntsMetadata) bool {
	if metadata.AntsInvocationId == "" {
		log.Printf("AntsInvocationId is not populated.")
		return true
	}

	if metadata.ParentWorkUnitId == "" {
		log.Printf("ParentWorkUnitId is not populated.")
		return true
	}

	if !isInternal(metadata.AccountId) {
		// Skip calling ants-publish for external partners.
		log.Printf("External partner accountId(%s) found.", metadata.AccountId)
		return true
	}

	return false
}

func skipTFUpload(req *api.InternalTestplan) {
	em := req.GetSuiteInfo().GetSuiteMetadata().GetExecutionMetadata()
	args := append(em.GetArgs(), &api.Arg{Flag: skipTFUploadFlag, Value: "true"})
	req.GetSuiteInfo().GetSuiteMetadata().GetExecutionMetadata().Args = args
}

func GeneratePublishTask(req *api.InternalTestplan, metadata *metadata.PublishAntsMetadata, publishPath string, log *log.Logger) error {
	if skipAntsPublish(metadata) {
		log.Printf("Skipping ants-publish task.")
		return nil
	}

	// Skip uploading to Ants using TF plugin.
	log.Printf("Skipping Ants upload through Tf plugin.")
	skipTFUpload(req)

	antsContainerBuilder := builders.NewContainerBuilder(
		containerID,  //  ContainerID
		"",           //  ContainerImageKey
		publishPath,  //  Container ImagePath
		artifactsDir, //  ContainerArtifactDir
		runCmd,
	)

	//  Add test artifacts directory for container.
	antsContainerBuilder.DynamicDeps = append(antsContainerBuilder.DynamicDeps,
		&api.DynamicDep{
			Key:   "generic.additionalVolumes",
			Value: "FMT=${env-TEMPDIR}:/tmp/artifacts",
		},
	)

	log.Printf("publishMetadata: %+v", metadata)
	publishRequestMetadata := &anypb.Any{}
	if err := publishRequestMetadata.MarshalFrom(metadata); err != nil {
		log.Printf("Failed to marshal request, %s", err)
	}
	log.Printf("publishRequestMetadata: %+v", publishRequestMetadata)

	dynamicDepsDefinition := defineDynamicDeps(antsContainerBuilder)
	log.Printf("dynamicDepsDefinition: %+v", dynamicDepsDefinition)

	generator := generators.NewInsertGenerator()
	generator.AddInsertion(
		&api.CrosTestRunnerDynamicRequest_Task{
			OrderedContainerRequests: []*api.ContainerRequest{
				antsContainerBuilder.Build(),
			},
			Task: &api.CrosTestRunnerDynamicRequest_Task_Publish{
				Publish: &api.PublishTask{
					ServiceAddress: &labapi.IpEndpoint{},
					PublishRequest: &api.PublishRequest{
						Metadata: publishRequestMetadata,
					},
					DynamicDeps:       dynamicDepsDefinition,
					DynamicIdentifier: dynamicIdentifier,
				},
			},
			Required: true,
		},
		dynamic_common.AppendTaskWrapper(dynamic_common.FindLast(api.FocalTaskFinder_PUBLISH)))

	// The dynamic updates are passed by reference and need the full path.
	// Do not use another var or substitution here.
	return dynamic_updates.AppendUserDefinedDynamicUpdates(&req.SuiteInfo.SuiteMetadata.DynamicUpdates, generator.Generate)
}

func defineDynamicDeps(antsContainerBuilder *builders.ContainerBuilder) []*api.DynamicDep {
	dynamicDeps := []*api.DynamicDep{
		{
			Key:   dynamic_common.ServiceAddress,
			Value: antsContainerBuilder.ContainerId,
		},
		{
			Key:   "publishRequest.testResponse",
			Value: "cros-test_runTests",
		},
		{
			Key:   "publishRequest.metadata.accountId",
			Value: "account-id",
		},
		{
			Key:   "publishRequest.metadata.luciInvocationId",
			Value: "invocation-id",
		},
		{
			Key:   "publishRequest.metadata.primaryExecutionInfo.dutInfo.dut",
			Value: "device_primary.dut",
		},
	}
	return dynamicDeps
}
