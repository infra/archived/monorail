// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package executors

import (
	"infra/cros/cmd/common_lib/interfaces"
)

// All supported executor types.
const (
	FilterExecutorType interfaces.ExecutorType = "FilterExecutor"
)
