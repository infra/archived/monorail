// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package clients

const (
	// Default flag values.
	UfsDev      string = "staging.ufs.api.cr.dev"
	UfsProd     string = "ufs.api.cr.dev"
	GcepDev     string = "gce-provider-dev.appspot.com"
	GcepProd    string = "gce-provider.appspot.com"
	ConfigID    string = "cloudbots-dev"
	SwarmingDev string = "chromium-swarm-dev.appspot.com"
)
