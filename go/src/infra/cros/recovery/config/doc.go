// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package config implements the control low of how Paris works with different devices or purposes(plans).
package config
