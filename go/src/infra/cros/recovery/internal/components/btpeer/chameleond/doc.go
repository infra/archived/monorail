// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package chameleond contains utilities for managing chameleond releases on
// btpeers devices.
package chameleond
