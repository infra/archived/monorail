// Copyright 2021 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.2
// 	protoc        v5.26.1
// source: infra/unifiedfleet/api/v1/models/chromeos/lab/siminfo.proto

package ufspb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Next Tag: 22
type NetworkProvider int32

const (
	NetworkProvider_NETWORK_OTHER       NetworkProvider = 0
	NetworkProvider_NETWORK_UNSUPPORTED NetworkProvider = 5
	NetworkProvider_NETWORK_TEST        NetworkProvider = 1
	NetworkProvider_NETWORK_ATT         NetworkProvider = 2
	NetworkProvider_NETWORK_TMOBILE     NetworkProvider = 3
	NetworkProvider_NETWORK_VERIZON     NetworkProvider = 4
	NetworkProvider_NETWORK_SPRINT      NetworkProvider = 6
	NetworkProvider_NETWORK_DOCOMO      NetworkProvider = 7
	NetworkProvider_NETWORK_SOFTBANK    NetworkProvider = 8
	NetworkProvider_NETWORK_KDDI        NetworkProvider = 9
	NetworkProvider_NETWORK_RAKUTEN     NetworkProvider = 10
	NetworkProvider_NETWORK_VODAFONE    NetworkProvider = 11
	NetworkProvider_NETWORK_EE          NetworkProvider = 12
	NetworkProvider_NETWORK_AMARISOFT   NetworkProvider = 13
	NetworkProvider_NETWORK_ROGER       NetworkProvider = 14
	NetworkProvider_NETWORK_BELL        NetworkProvider = 15
	NetworkProvider_NETWORK_TELUS       NetworkProvider = 16
	NetworkProvider_NETWORK_FI          NetworkProvider = 17
	NetworkProvider_NETWORK_CBRS        NetworkProvider = 18
	NetworkProvider_NETWORK_LINEMO      NetworkProvider = 19
	NetworkProvider_NETWORK_POVO        NetworkProvider = 20
	NetworkProvider_NETWORK_HANSHIN     NetworkProvider = 21
)

// Enum value maps for NetworkProvider.
var (
	NetworkProvider_name = map[int32]string{
		0:  "NETWORK_OTHER",
		5:  "NETWORK_UNSUPPORTED",
		1:  "NETWORK_TEST",
		2:  "NETWORK_ATT",
		3:  "NETWORK_TMOBILE",
		4:  "NETWORK_VERIZON",
		6:  "NETWORK_SPRINT",
		7:  "NETWORK_DOCOMO",
		8:  "NETWORK_SOFTBANK",
		9:  "NETWORK_KDDI",
		10: "NETWORK_RAKUTEN",
		11: "NETWORK_VODAFONE",
		12: "NETWORK_EE",
		13: "NETWORK_AMARISOFT",
		14: "NETWORK_ROGER",
		15: "NETWORK_BELL",
		16: "NETWORK_TELUS",
		17: "NETWORK_FI",
		18: "NETWORK_CBRS",
		19: "NETWORK_LINEMO",
		20: "NETWORK_POVO",
		21: "NETWORK_HANSHIN",
	}
	NetworkProvider_value = map[string]int32{
		"NETWORK_OTHER":       0,
		"NETWORK_UNSUPPORTED": 5,
		"NETWORK_TEST":        1,
		"NETWORK_ATT":         2,
		"NETWORK_TMOBILE":     3,
		"NETWORK_VERIZON":     4,
		"NETWORK_SPRINT":      6,
		"NETWORK_DOCOMO":      7,
		"NETWORK_SOFTBANK":    8,
		"NETWORK_KDDI":        9,
		"NETWORK_RAKUTEN":     10,
		"NETWORK_VODAFONE":    11,
		"NETWORK_EE":          12,
		"NETWORK_AMARISOFT":   13,
		"NETWORK_ROGER":       14,
		"NETWORK_BELL":        15,
		"NETWORK_TELUS":       16,
		"NETWORK_FI":          17,
		"NETWORK_CBRS":        18,
		"NETWORK_LINEMO":      19,
		"NETWORK_POVO":        20,
		"NETWORK_HANSHIN":     21,
	}
)

func (x NetworkProvider) Enum() *NetworkProvider {
	p := new(NetworkProvider)
	*p = x
	return p
}

func (x NetworkProvider) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (NetworkProvider) Descriptor() protoreflect.EnumDescriptor {
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_enumTypes[0].Descriptor()
}

func (NetworkProvider) Type() protoreflect.EnumType {
	return &file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_enumTypes[0]
}

func (x NetworkProvider) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use NetworkProvider.Descriptor instead.
func (NetworkProvider) EnumDescriptor() ([]byte, []int) {
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescGZIP(), []int{0}
}

// Next Tag: 3
type SIMType int32

const (
	SIMType_SIM_UNKNOWN  SIMType = 0
	SIMType_SIM_PHYSICAL SIMType = 1
	SIMType_SIM_DIGITAL  SIMType = 2
)

// Enum value maps for SIMType.
var (
	SIMType_name = map[int32]string{
		0: "SIM_UNKNOWN",
		1: "SIM_PHYSICAL",
		2: "SIM_DIGITAL",
	}
	SIMType_value = map[string]int32{
		"SIM_UNKNOWN":  0,
		"SIM_PHYSICAL": 1,
		"SIM_DIGITAL":  2,
	}
)

func (x SIMType) Enum() *SIMType {
	p := new(SIMType)
	*p = x
	return p
}

func (x SIMType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SIMType) Descriptor() protoreflect.EnumDescriptor {
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_enumTypes[1].Descriptor()
}

func (SIMType) Type() protoreflect.EnumType {
	return &file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_enumTypes[1]
}

func (x SIMType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SIMType.Descriptor instead.
func (SIMType) EnumDescriptor() ([]byte, []int) {
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescGZIP(), []int{1}
}

// Possible states of the SIM profile.
type SIMProfileInfo_State int32

const (
	// State not set.
	SIMProfileInfo_UNSPECIFIED SIMProfileInfo_State = 0
	// The device is unusable.
	SIMProfileInfo_BROKEN SIMProfileInfo_State = 1
	// The device needs to be unlocked.
	SIMProfileInfo_LOCKED SIMProfileInfo_State = 2
	// No data connection available and not in a failed state.
	SIMProfileInfo_NO_NETWORK SIMProfileInfo_State = 3
	// The device is registered with a network provider, and data connections and messaging may be available for use.
	SIMProfileInfo_WORKING SIMProfileInfo_State = 4
	// The device has an invalid configuration in UFS.
	SIMProfileInfo_WRONG_CONFIG SIMProfileInfo_State = 5
)

// Enum value maps for SIMProfileInfo_State.
var (
	SIMProfileInfo_State_name = map[int32]string{
		0: "UNSPECIFIED",
		1: "BROKEN",
		2: "LOCKED",
		3: "NO_NETWORK",
		4: "WORKING",
		5: "WRONG_CONFIG",
	}
	SIMProfileInfo_State_value = map[string]int32{
		"UNSPECIFIED":  0,
		"BROKEN":       1,
		"LOCKED":       2,
		"NO_NETWORK":   3,
		"WORKING":      4,
		"WRONG_CONFIG": 5,
	}
)

func (x SIMProfileInfo_State) Enum() *SIMProfileInfo_State {
	p := new(SIMProfileInfo_State)
	*p = x
	return p
}

func (x SIMProfileInfo_State) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SIMProfileInfo_State) Descriptor() protoreflect.EnumDescriptor {
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_enumTypes[2].Descriptor()
}

func (SIMProfileInfo_State) Type() protoreflect.EnumType {
	return &file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_enumTypes[2]
}

func (x SIMProfileInfo_State) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SIMProfileInfo_State.Descriptor instead.
func (SIMProfileInfo_State) EnumDescriptor() ([]byte, []int) {
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescGZIP(), []int{1, 0}
}

// Possible features that the SIM supports.
type SIMProfileInfo_Feature int32

const (
	// Unset feature.
	SIMProfileInfo_FEATURE_UNSPECIFIED SIMProfileInfo_Feature = 0
	// The SIM supports a generic live network.
	SIMProfileInfo_FEATURE_LIVE_NETWORK SIMProfileInfo_Feature = 1
	// The SIM supports SMS messaging.
	SIMProfileInfo_FEATURE_SMS SIMProfileInfo_Feature = 2
)

// Enum value maps for SIMProfileInfo_Feature.
var (
	SIMProfileInfo_Feature_name = map[int32]string{
		0: "FEATURE_UNSPECIFIED",
		1: "FEATURE_LIVE_NETWORK",
		2: "FEATURE_SMS",
	}
	SIMProfileInfo_Feature_value = map[string]int32{
		"FEATURE_UNSPECIFIED":  0,
		"FEATURE_LIVE_NETWORK": 1,
		"FEATURE_SMS":          2,
	}
)

func (x SIMProfileInfo_Feature) Enum() *SIMProfileInfo_Feature {
	p := new(SIMProfileInfo_Feature)
	*p = x
	return p
}

func (x SIMProfileInfo_Feature) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SIMProfileInfo_Feature) Descriptor() protoreflect.EnumDescriptor {
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_enumTypes[3].Descriptor()
}

func (SIMProfileInfo_Feature) Type() protoreflect.EnumType {
	return &file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_enumTypes[3]
}

func (x SIMProfileInfo_Feature) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SIMProfileInfo_Feature.Descriptor instead.
func (SIMProfileInfo_Feature) EnumDescriptor() ([]byte, []int) {
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescGZIP(), []int{1, 1}
}

// Next Tag: 6
type SIMInfo struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	SlotId      int32             `protobuf:"varint,1,opt,name=slot_id,json=slotId,proto3" json:"slot_id,omitempty"`
	Type        SIMType           `protobuf:"varint,2,opt,name=type,proto3,enum=unifiedfleet.api.v1.models.chromeos.lab.SIMType" json:"type,omitempty"`
	Eid         string            `protobuf:"bytes,3,opt,name=eid,proto3" json:"eid,omitempty"`
	TestEsim    bool              `protobuf:"varint,4,opt,name=test_esim,json=testEsim,proto3" json:"test_esim,omitempty"`
	ProfileInfo []*SIMProfileInfo `protobuf:"bytes,5,rep,name=profile_info,json=profileInfo,proto3" json:"profile_info,omitempty"`
}

func (x *SIMInfo) Reset() {
	*x = SIMInfo{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SIMInfo) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SIMInfo) ProtoMessage() {}

func (x *SIMInfo) ProtoReflect() protoreflect.Message {
	mi := &file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SIMInfo.ProtoReflect.Descriptor instead.
func (*SIMInfo) Descriptor() ([]byte, []int) {
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescGZIP(), []int{0}
}

func (x *SIMInfo) GetSlotId() int32 {
	if x != nil {
		return x.SlotId
	}
	return 0
}

func (x *SIMInfo) GetType() SIMType {
	if x != nil {
		return x.Type
	}
	return SIMType_SIM_UNKNOWN
}

func (x *SIMInfo) GetEid() string {
	if x != nil {
		return x.Eid
	}
	return ""
}

func (x *SIMInfo) GetTestEsim() bool {
	if x != nil {
		return x.TestEsim
	}
	return false
}

func (x *SIMInfo) GetProfileInfo() []*SIMProfileInfo {
	if x != nil {
		return x.ProfileInfo
	}
	return nil
}

// Next Tag: 7
type SIMProfileInfo struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Iccid       string          `protobuf:"bytes,1,opt,name=iccid,proto3" json:"iccid,omitempty"`
	SimPin      string          `protobuf:"bytes,2,opt,name=sim_pin,json=simPin,proto3" json:"sim_pin,omitempty"`
	SimPuk      string          `protobuf:"bytes,3,opt,name=sim_puk,json=simPuk,proto3" json:"sim_puk,omitempty"`
	CarrierName NetworkProvider `protobuf:"varint,4,opt,name=carrier_name,json=carrierName,proto3,enum=unifiedfleet.api.v1.models.chromeos.lab.NetworkProvider" json:"carrier_name,omitempty"`
	OwnNumber   string          `protobuf:"bytes,5,opt,name=own_number,json=ownNumber,proto3" json:"own_number,omitempty"`
	// The SIM state as reported by the cellular modem.
	State SIMProfileInfo_State `protobuf:"varint,6,opt,name=state,proto3,enum=unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo_State" json:"state,omitempty"`
	// Features supported by the profile.
	// These features are used to determine what tests can be run against which SIMs
	// in the lab, see go/cros-cellular-features for more information.
	// File bugs against buganizer component: 979102.
	Features []SIMProfileInfo_Feature `protobuf:"varint,7,rep,packed,name=features,proto3,enum=unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo_Feature" json:"features,omitempty"`
}

func (x *SIMProfileInfo) Reset() {
	*x = SIMProfileInfo{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SIMProfileInfo) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SIMProfileInfo) ProtoMessage() {}

func (x *SIMProfileInfo) ProtoReflect() protoreflect.Message {
	mi := &file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SIMProfileInfo.ProtoReflect.Descriptor instead.
func (*SIMProfileInfo) Descriptor() ([]byte, []int) {
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescGZIP(), []int{1}
}

func (x *SIMProfileInfo) GetIccid() string {
	if x != nil {
		return x.Iccid
	}
	return ""
}

func (x *SIMProfileInfo) GetSimPin() string {
	if x != nil {
		return x.SimPin
	}
	return ""
}

func (x *SIMProfileInfo) GetSimPuk() string {
	if x != nil {
		return x.SimPuk
	}
	return ""
}

func (x *SIMProfileInfo) GetCarrierName() NetworkProvider {
	if x != nil {
		return x.CarrierName
	}
	return NetworkProvider_NETWORK_OTHER
}

func (x *SIMProfileInfo) GetOwnNumber() string {
	if x != nil {
		return x.OwnNumber
	}
	return ""
}

func (x *SIMProfileInfo) GetState() SIMProfileInfo_State {
	if x != nil {
		return x.State
	}
	return SIMProfileInfo_UNSPECIFIED
}

func (x *SIMProfileInfo) GetFeatures() []SIMProfileInfo_Feature {
	if x != nil {
		return x.Features
	}
	return nil
}

var File_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto protoreflect.FileDescriptor

var file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDesc = []byte{
	0x0a, 0x3b, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64, 0x66,
	0x6c, 0x65, 0x65, 0x74, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x6f, 0x64, 0x65,
	0x6c, 0x73, 0x2f, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x65, 0x6f, 0x73, 0x2f, 0x6c, 0x61, 0x62, 0x2f,
	0x73, 0x69, 0x6d, 0x69, 0x6e, 0x66, 0x6f, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x27, 0x75,
	0x6e, 0x69, 0x66, 0x69, 0x65, 0x64, 0x66, 0x6c, 0x65, 0x65, 0x74, 0x2e, 0x61, 0x70, 0x69, 0x2e,
	0x76, 0x31, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x73, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x65,
	0x6f, 0x73, 0x2e, 0x6c, 0x61, 0x62, 0x22, 0xf3, 0x01, 0x0a, 0x07, 0x53, 0x49, 0x4d, 0x49, 0x6e,
	0x66, 0x6f, 0x12, 0x17, 0x0a, 0x07, 0x73, 0x6c, 0x6f, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x06, 0x73, 0x6c, 0x6f, 0x74, 0x49, 0x64, 0x12, 0x44, 0x0a, 0x04, 0x74,
	0x79, 0x70, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x30, 0x2e, 0x75, 0x6e, 0x69, 0x66,
	0x69, 0x65, 0x64, 0x66, 0x6c, 0x65, 0x65, 0x74, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e,
	0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x73, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x65, 0x6f, 0x73, 0x2e,
	0x6c, 0x61, 0x62, 0x2e, 0x53, 0x49, 0x4d, 0x54, 0x79, 0x70, 0x65, 0x52, 0x04, 0x74, 0x79, 0x70,
	0x65, 0x12, 0x10, 0x0a, 0x03, 0x65, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03,
	0x65, 0x69, 0x64, 0x12, 0x1b, 0x0a, 0x09, 0x74, 0x65, 0x73, 0x74, 0x5f, 0x65, 0x73, 0x69, 0x6d,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x08, 0x52, 0x08, 0x74, 0x65, 0x73, 0x74, 0x45, 0x73, 0x69, 0x6d,
	0x12, 0x5a, 0x0a, 0x0c, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x5f, 0x69, 0x6e, 0x66, 0x6f,
	0x18, 0x05, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x37, 0x2e, 0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64,
	0x66, 0x6c, 0x65, 0x65, 0x74, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x6d, 0x6f, 0x64,
	0x65, 0x6c, 0x73, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x65, 0x6f, 0x73, 0x2e, 0x6c, 0x61, 0x62,
	0x2e, 0x53, 0x49, 0x4d, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x6e, 0x66, 0x6f, 0x52,
	0x0b, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x6e, 0x66, 0x6f, 0x22, 0xb6, 0x04, 0x0a,
	0x0e, 0x53, 0x49, 0x4d, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x6e, 0x66, 0x6f, 0x12,
	0x14, 0x0a, 0x05, 0x69, 0x63, 0x63, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05,
	0x69, 0x63, 0x63, 0x69, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x73, 0x69, 0x6d, 0x5f, 0x70, 0x69, 0x6e,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x69, 0x6d, 0x50, 0x69, 0x6e, 0x12, 0x17,
	0x0a, 0x07, 0x73, 0x69, 0x6d, 0x5f, 0x70, 0x75, 0x6b, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x06, 0x73, 0x69, 0x6d, 0x50, 0x75, 0x6b, 0x12, 0x5b, 0x0a, 0x0c, 0x63, 0x61, 0x72, 0x72, 0x69,
	0x65, 0x72, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x38, 0x2e,
	0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64, 0x66, 0x6c, 0x65, 0x65, 0x74, 0x2e, 0x61, 0x70, 0x69,
	0x2e, 0x76, 0x31, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x73, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d,
	0x65, 0x6f, 0x73, 0x2e, 0x6c, 0x61, 0x62, 0x2e, 0x4e, 0x65, 0x74, 0x77, 0x6f, 0x72, 0x6b, 0x50,
	0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x52, 0x0b, 0x63, 0x61, 0x72, 0x72, 0x69, 0x65, 0x72,
	0x4e, 0x61, 0x6d, 0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x6f, 0x77, 0x6e, 0x5f, 0x6e, 0x75, 0x6d, 0x62,
	0x65, 0x72, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x6f, 0x77, 0x6e, 0x4e, 0x75, 0x6d,
	0x62, 0x65, 0x72, 0x12, 0x53, 0x0a, 0x05, 0x73, 0x74, 0x61, 0x74, 0x65, 0x18, 0x06, 0x20, 0x01,
	0x28, 0x0e, 0x32, 0x3d, 0x2e, 0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64, 0x66, 0x6c, 0x65, 0x65,
	0x74, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x73, 0x2e,
	0x63, 0x68, 0x72, 0x6f, 0x6d, 0x65, 0x6f, 0x73, 0x2e, 0x6c, 0x61, 0x62, 0x2e, 0x53, 0x49, 0x4d,
	0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49, 0x6e, 0x66, 0x6f, 0x2e, 0x53, 0x74, 0x61, 0x74,
	0x65, 0x52, 0x05, 0x73, 0x74, 0x61, 0x74, 0x65, 0x12, 0x5b, 0x0a, 0x08, 0x66, 0x65, 0x61, 0x74,
	0x75, 0x72, 0x65, 0x73, 0x18, 0x07, 0x20, 0x03, 0x28, 0x0e, 0x32, 0x3f, 0x2e, 0x75, 0x6e, 0x69,
	0x66, 0x69, 0x65, 0x64, 0x66, 0x6c, 0x65, 0x65, 0x74, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31,
	0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x73, 0x2e, 0x63, 0x68, 0x72, 0x6f, 0x6d, 0x65, 0x6f, 0x73,
	0x2e, 0x6c, 0x61, 0x62, 0x2e, 0x53, 0x49, 0x4d, 0x50, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49,
	0x6e, 0x66, 0x6f, 0x2e, 0x46, 0x65, 0x61, 0x74, 0x75, 0x72, 0x65, 0x52, 0x08, 0x66, 0x65, 0x61,
	0x74, 0x75, 0x72, 0x65, 0x73, 0x22, 0x5f, 0x0a, 0x05, 0x53, 0x74, 0x61, 0x74, 0x65, 0x12, 0x0f,
	0x0a, 0x0b, 0x55, 0x4e, 0x53, 0x50, 0x45, 0x43, 0x49, 0x46, 0x49, 0x45, 0x44, 0x10, 0x00, 0x12,
	0x0a, 0x0a, 0x06, 0x42, 0x52, 0x4f, 0x4b, 0x45, 0x4e, 0x10, 0x01, 0x12, 0x0a, 0x0a, 0x06, 0x4c,
	0x4f, 0x43, 0x4b, 0x45, 0x44, 0x10, 0x02, 0x12, 0x0e, 0x0a, 0x0a, 0x4e, 0x4f, 0x5f, 0x4e, 0x45,
	0x54, 0x57, 0x4f, 0x52, 0x4b, 0x10, 0x03, 0x12, 0x0b, 0x0a, 0x07, 0x57, 0x4f, 0x52, 0x4b, 0x49,
	0x4e, 0x47, 0x10, 0x04, 0x12, 0x10, 0x0a, 0x0c, 0x57, 0x52, 0x4f, 0x4e, 0x47, 0x5f, 0x43, 0x4f,
	0x4e, 0x46, 0x49, 0x47, 0x10, 0x05, 0x22, 0x4d, 0x0a, 0x07, 0x46, 0x65, 0x61, 0x74, 0x75, 0x72,
	0x65, 0x12, 0x17, 0x0a, 0x13, 0x46, 0x45, 0x41, 0x54, 0x55, 0x52, 0x45, 0x5f, 0x55, 0x4e, 0x53,
	0x50, 0x45, 0x43, 0x49, 0x46, 0x49, 0x45, 0x44, 0x10, 0x00, 0x12, 0x18, 0x0a, 0x14, 0x46, 0x45,
	0x41, 0x54, 0x55, 0x52, 0x45, 0x5f, 0x4c, 0x49, 0x56, 0x45, 0x5f, 0x4e, 0x45, 0x54, 0x57, 0x4f,
	0x52, 0x4b, 0x10, 0x01, 0x12, 0x0f, 0x0a, 0x0b, 0x46, 0x45, 0x41, 0x54, 0x55, 0x52, 0x45, 0x5f,
	0x53, 0x4d, 0x53, 0x10, 0x02, 0x2a, 0xc1, 0x03, 0x0a, 0x0f, 0x4e, 0x65, 0x74, 0x77, 0x6f, 0x72,
	0x6b, 0x50, 0x72, 0x6f, 0x76, 0x69, 0x64, 0x65, 0x72, 0x12, 0x11, 0x0a, 0x0d, 0x4e, 0x45, 0x54,
	0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x4f, 0x54, 0x48, 0x45, 0x52, 0x10, 0x00, 0x12, 0x17, 0x0a, 0x13,
	0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x55, 0x4e, 0x53, 0x55, 0x50, 0x50, 0x4f, 0x52,
	0x54, 0x45, 0x44, 0x10, 0x05, 0x12, 0x10, 0x0a, 0x0c, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b,
	0x5f, 0x54, 0x45, 0x53, 0x54, 0x10, 0x01, 0x12, 0x0f, 0x0a, 0x0b, 0x4e, 0x45, 0x54, 0x57, 0x4f,
	0x52, 0x4b, 0x5f, 0x41, 0x54, 0x54, 0x10, 0x02, 0x12, 0x13, 0x0a, 0x0f, 0x4e, 0x45, 0x54, 0x57,
	0x4f, 0x52, 0x4b, 0x5f, 0x54, 0x4d, 0x4f, 0x42, 0x49, 0x4c, 0x45, 0x10, 0x03, 0x12, 0x13, 0x0a,
	0x0f, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x56, 0x45, 0x52, 0x49, 0x5a, 0x4f, 0x4e,
	0x10, 0x04, 0x12, 0x12, 0x0a, 0x0e, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x53, 0x50,
	0x52, 0x49, 0x4e, 0x54, 0x10, 0x06, 0x12, 0x12, 0x0a, 0x0e, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52,
	0x4b, 0x5f, 0x44, 0x4f, 0x43, 0x4f, 0x4d, 0x4f, 0x10, 0x07, 0x12, 0x14, 0x0a, 0x10, 0x4e, 0x45,
	0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x53, 0x4f, 0x46, 0x54, 0x42, 0x41, 0x4e, 0x4b, 0x10, 0x08,
	0x12, 0x10, 0x0a, 0x0c, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x4b, 0x44, 0x44, 0x49,
	0x10, 0x09, 0x12, 0x13, 0x0a, 0x0f, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x52, 0x41,
	0x4b, 0x55, 0x54, 0x45, 0x4e, 0x10, 0x0a, 0x12, 0x14, 0x0a, 0x10, 0x4e, 0x45, 0x54, 0x57, 0x4f,
	0x52, 0x4b, 0x5f, 0x56, 0x4f, 0x44, 0x41, 0x46, 0x4f, 0x4e, 0x45, 0x10, 0x0b, 0x12, 0x0e, 0x0a,
	0x0a, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x45, 0x45, 0x10, 0x0c, 0x12, 0x15, 0x0a,
	0x11, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x41, 0x4d, 0x41, 0x52, 0x49, 0x53, 0x4f,
	0x46, 0x54, 0x10, 0x0d, 0x12, 0x11, 0x0a, 0x0d, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f,
	0x52, 0x4f, 0x47, 0x45, 0x52, 0x10, 0x0e, 0x12, 0x10, 0x0a, 0x0c, 0x4e, 0x45, 0x54, 0x57, 0x4f,
	0x52, 0x4b, 0x5f, 0x42, 0x45, 0x4c, 0x4c, 0x10, 0x0f, 0x12, 0x11, 0x0a, 0x0d, 0x4e, 0x45, 0x54,
	0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x54, 0x45, 0x4c, 0x55, 0x53, 0x10, 0x10, 0x12, 0x0e, 0x0a, 0x0a,
	0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x46, 0x49, 0x10, 0x11, 0x12, 0x10, 0x0a, 0x0c,
	0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x43, 0x42, 0x52, 0x53, 0x10, 0x12, 0x12, 0x12,
	0x0a, 0x0e, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x4c, 0x49, 0x4e, 0x45, 0x4d, 0x4f,
	0x10, 0x13, 0x12, 0x10, 0x0a, 0x0c, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f, 0x50, 0x4f,
	0x56, 0x4f, 0x10, 0x14, 0x12, 0x13, 0x0a, 0x0f, 0x4e, 0x45, 0x54, 0x57, 0x4f, 0x52, 0x4b, 0x5f,
	0x48, 0x41, 0x4e, 0x53, 0x48, 0x49, 0x4e, 0x10, 0x15, 0x2a, 0x3d, 0x0a, 0x07, 0x53, 0x49, 0x4d,
	0x54, 0x79, 0x70, 0x65, 0x12, 0x0f, 0x0a, 0x0b, 0x53, 0x49, 0x4d, 0x5f, 0x55, 0x4e, 0x4b, 0x4e,
	0x4f, 0x57, 0x4e, 0x10, 0x00, 0x12, 0x10, 0x0a, 0x0c, 0x53, 0x49, 0x4d, 0x5f, 0x50, 0x48, 0x59,
	0x53, 0x49, 0x43, 0x41, 0x4c, 0x10, 0x01, 0x12, 0x0f, 0x0a, 0x0b, 0x53, 0x49, 0x4d, 0x5f, 0x44,
	0x49, 0x47, 0x49, 0x54, 0x41, 0x4c, 0x10, 0x02, 0x42, 0x35, 0x5a, 0x33, 0x69, 0x6e, 0x66, 0x72,
	0x61, 0x2f, 0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64, 0x66, 0x6c, 0x65, 0x65, 0x74, 0x2f, 0x61,
	0x70, 0x69, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x73, 0x2f, 0x63, 0x68, 0x72,
	0x6f, 0x6d, 0x65, 0x6f, 0x73, 0x2f, 0x6c, 0x61, 0x62, 0x3b, 0x75, 0x66, 0x73, 0x70, 0x62, 0x62,
	0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescOnce sync.Once
	file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescData = file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDesc
)

func file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescGZIP() []byte {
	file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescOnce.Do(func() {
		file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescData = protoimpl.X.CompressGZIP(file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescData)
	})
	return file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDescData
}

var file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_enumTypes = make([]protoimpl.EnumInfo, 4)
var file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_goTypes = []any{
	(NetworkProvider)(0),        // 0: unifiedfleet.api.v1.models.chromeos.lab.NetworkProvider
	(SIMType)(0),                // 1: unifiedfleet.api.v1.models.chromeos.lab.SIMType
	(SIMProfileInfo_State)(0),   // 2: unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo.State
	(SIMProfileInfo_Feature)(0), // 3: unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo.Feature
	(*SIMInfo)(nil),             // 4: unifiedfleet.api.v1.models.chromeos.lab.SIMInfo
	(*SIMProfileInfo)(nil),      // 5: unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo
}
var file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_depIdxs = []int32{
	1, // 0: unifiedfleet.api.v1.models.chromeos.lab.SIMInfo.type:type_name -> unifiedfleet.api.v1.models.chromeos.lab.SIMType
	5, // 1: unifiedfleet.api.v1.models.chromeos.lab.SIMInfo.profile_info:type_name -> unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo
	0, // 2: unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo.carrier_name:type_name -> unifiedfleet.api.v1.models.chromeos.lab.NetworkProvider
	2, // 3: unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo.state:type_name -> unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo.State
	3, // 4: unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo.features:type_name -> unifiedfleet.api.v1.models.chromeos.lab.SIMProfileInfo.Feature
	5, // [5:5] is the sub-list for method output_type
	5, // [5:5] is the sub-list for method input_type
	5, // [5:5] is the sub-list for extension type_name
	5, // [5:5] is the sub-list for extension extendee
	0, // [0:5] is the sub-list for field type_name
}

func init() { file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_init() }
func file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_init() {
	if File_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_msgTypes[0].Exporter = func(v any, i int) any {
			switch v := v.(*SIMInfo); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_msgTypes[1].Exporter = func(v any, i int) any {
			switch v := v.(*SIMProfileInfo); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDesc,
			NumEnums:      4,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_goTypes,
		DependencyIndexes: file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_depIdxs,
		EnumInfos:         file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_enumTypes,
		MessageInfos:      file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_msgTypes,
	}.Build()
	File_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto = out.File
	file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_rawDesc = nil
	file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_goTypes = nil
	file_infra_unifiedfleet_api_v1_models_chromeos_lab_siminfo_proto_depIdxs = nil
}
