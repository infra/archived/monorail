// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.34.2
// 	protoc        v5.26.1
// source: infra/unifiedfleet/api/v1/models/secret.proto

package ufspb

import (
	_ "google.golang.org/genproto/googleapis/api/annotations"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Next Tag: 3
// DefaultWifi is the default wifi setting in a scope indicated by the name.
type DefaultWifi struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// The resource name of a default wifi setting in a scope.
	// Format: defaultwifis/{defaultwifi}
	// The default wifi is named after the zone name or pool name, but in lower
	// case to follow https://google.aip.dev/122.
	// If the name starts with "zone_", we think it's for a UFS zone as all UFS
	// zones are prefixed with "ZONE_". Otherwise, it's for a DUT pool.
	// Example: "defaultwifis/zone_sfo36_os" (for a UFS zone),
	// Example: "defaultwifis/wifi-pool" (for a DUT pool).
	Name       string  `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	WifiSecret *Secret `protobuf:"bytes,2,opt,name=wifi_secret,json=wifiSecret,proto3" json:"wifi_secret,omitempty"`
}

func (x *DefaultWifi) Reset() {
	*x = DefaultWifi{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infra_unifiedfleet_api_v1_models_secret_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DefaultWifi) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DefaultWifi) ProtoMessage() {}

func (x *DefaultWifi) ProtoReflect() protoreflect.Message {
	mi := &file_infra_unifiedfleet_api_v1_models_secret_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DefaultWifi.ProtoReflect.Descriptor instead.
func (*DefaultWifi) Descriptor() ([]byte, []int) {
	return file_infra_unifiedfleet_api_v1_models_secret_proto_rawDescGZIP(), []int{0}
}

func (x *DefaultWifi) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *DefaultWifi) GetWifiSecret() *Secret {
	if x != nil {
		return x.WifiSecret
	}
	return nil
}

// Next Tag: 3
// Secret is the secret stored/managed in the Secret Manager of a GCP
// project.
type Secret struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// The GCP project storing the secret, default is 'unifiedfleet', i.e. UFS.
	ProjectId  string `protobuf:"bytes,1,opt,name=project_id,json=projectId,proto3" json:"project_id,omitempty"`
	SecretName string `protobuf:"bytes,2,opt,name=secret_name,json=secretName,proto3" json:"secret_name,omitempty"` // The secret name in the Secret Manager.
}

func (x *Secret) Reset() {
	*x = Secret{}
	if protoimpl.UnsafeEnabled {
		mi := &file_infra_unifiedfleet_api_v1_models_secret_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Secret) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Secret) ProtoMessage() {}

func (x *Secret) ProtoReflect() protoreflect.Message {
	mi := &file_infra_unifiedfleet_api_v1_models_secret_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Secret.ProtoReflect.Descriptor instead.
func (*Secret) Descriptor() ([]byte, []int) {
	return file_infra_unifiedfleet_api_v1_models_secret_proto_rawDescGZIP(), []int{1}
}

func (x *Secret) GetProjectId() string {
	if x != nil {
		return x.ProjectId
	}
	return ""
}

func (x *Secret) GetSecretName() string {
	if x != nil {
		return x.SecretName
	}
	return ""
}

var File_infra_unifiedfleet_api_v1_models_secret_proto protoreflect.FileDescriptor

var file_infra_unifiedfleet_api_v1_models_secret_proto_rawDesc = []byte{
	0x0a, 0x2d, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64, 0x66,
	0x6c, 0x65, 0x65, 0x74, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x6f, 0x64, 0x65,
	0x6c, 0x73, 0x2f, 0x73, 0x65, 0x63, 0x72, 0x65, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x1a, 0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64, 0x66, 0x6c, 0x65, 0x65, 0x74, 0x2e, 0x61, 0x70,
	0x69, 0x2e, 0x76, 0x31, 0x2e, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x73, 0x1a, 0x1f, 0x67, 0x6f, 0x6f,
	0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x66, 0x69, 0x65, 0x6c, 0x64, 0x5f, 0x62, 0x65,
	0x68, 0x61, 0x76, 0x69, 0x6f, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x19, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x72, 0x65, 0x73, 0x6f, 0x75, 0x72, 0x63,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xba, 0x01, 0x0a, 0x0b, 0x44, 0x65, 0x66, 0x61,
	0x75, 0x6c, 0x74, 0x57, 0x69, 0x66, 0x69, 0x12, 0x17, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x42, 0x03, 0xe0, 0x41, 0x08, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65,
	0x12, 0x43, 0x0a, 0x0b, 0x77, 0x69, 0x66, 0x69, 0x5f, 0x73, 0x65, 0x63, 0x72, 0x65, 0x74, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x22, 0x2e, 0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64, 0x66,
	0x6c, 0x65, 0x65, 0x74, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x76, 0x31, 0x2e, 0x6d, 0x6f, 0x64, 0x65,
	0x6c, 0x73, 0x2e, 0x53, 0x65, 0x63, 0x72, 0x65, 0x74, 0x52, 0x0a, 0x77, 0x69, 0x66, 0x69, 0x53,
	0x65, 0x63, 0x72, 0x65, 0x74, 0x3a, 0x4d, 0xea, 0x41, 0x4a, 0x0a, 0x2c, 0x75, 0x6e, 0x69, 0x66,
	0x69, 0x65, 0x64, 0x2d, 0x66, 0x6c, 0x65, 0x65, 0x74, 0x2d, 0x73, 0x79, 0x73, 0x74, 0x65, 0x6d,
	0x2e, 0x61, 0x70, 0x70, 0x73, 0x70, 0x6f, 0x74, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x44, 0x65, 0x66,
	0x61, 0x75, 0x6c, 0x74, 0x57, 0x69, 0x66, 0x69, 0x12, 0x1a, 0x64, 0x65, 0x66, 0x61, 0x75, 0x6c,
	0x74, 0x77, 0x69, 0x66, 0x69, 0x73, 0x2f, 0x7b, 0x64, 0x65, 0x66, 0x61, 0x75, 0x6c, 0x74, 0x77,
	0x69, 0x66, 0x69, 0x7d, 0x22, 0x48, 0x0a, 0x06, 0x53, 0x65, 0x63, 0x72, 0x65, 0x74, 0x12, 0x1d,
	0x0a, 0x0a, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x09, 0x70, 0x72, 0x6f, 0x6a, 0x65, 0x63, 0x74, 0x49, 0x64, 0x12, 0x1f, 0x0a,
	0x0b, 0x73, 0x65, 0x63, 0x72, 0x65, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0a, 0x73, 0x65, 0x63, 0x72, 0x65, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x42, 0x28,
	0x5a, 0x26, 0x69, 0x6e, 0x66, 0x72, 0x61, 0x2f, 0x75, 0x6e, 0x69, 0x66, 0x69, 0x65, 0x64, 0x66,
	0x6c, 0x65, 0x65, 0x74, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x76, 0x31, 0x2f, 0x6d, 0x6f, 0x64, 0x65,
	0x6c, 0x73, 0x3b, 0x75, 0x66, 0x73, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_infra_unifiedfleet_api_v1_models_secret_proto_rawDescOnce sync.Once
	file_infra_unifiedfleet_api_v1_models_secret_proto_rawDescData = file_infra_unifiedfleet_api_v1_models_secret_proto_rawDesc
)

func file_infra_unifiedfleet_api_v1_models_secret_proto_rawDescGZIP() []byte {
	file_infra_unifiedfleet_api_v1_models_secret_proto_rawDescOnce.Do(func() {
		file_infra_unifiedfleet_api_v1_models_secret_proto_rawDescData = protoimpl.X.CompressGZIP(file_infra_unifiedfleet_api_v1_models_secret_proto_rawDescData)
	})
	return file_infra_unifiedfleet_api_v1_models_secret_proto_rawDescData
}

var file_infra_unifiedfleet_api_v1_models_secret_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_infra_unifiedfleet_api_v1_models_secret_proto_goTypes = []any{
	(*DefaultWifi)(nil), // 0: unifiedfleet.api.v1.models.DefaultWifi
	(*Secret)(nil),      // 1: unifiedfleet.api.v1.models.Secret
}
var file_infra_unifiedfleet_api_v1_models_secret_proto_depIdxs = []int32{
	1, // 0: unifiedfleet.api.v1.models.DefaultWifi.wifi_secret:type_name -> unifiedfleet.api.v1.models.Secret
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_infra_unifiedfleet_api_v1_models_secret_proto_init() }
func file_infra_unifiedfleet_api_v1_models_secret_proto_init() {
	if File_infra_unifiedfleet_api_v1_models_secret_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_infra_unifiedfleet_api_v1_models_secret_proto_msgTypes[0].Exporter = func(v any, i int) any {
			switch v := v.(*DefaultWifi); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_infra_unifiedfleet_api_v1_models_secret_proto_msgTypes[1].Exporter = func(v any, i int) any {
			switch v := v.(*Secret); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_infra_unifiedfleet_api_v1_models_secret_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_infra_unifiedfleet_api_v1_models_secret_proto_goTypes,
		DependencyIndexes: file_infra_unifiedfleet_api_v1_models_secret_proto_depIdxs,
		MessageInfos:      file_infra_unifiedfleet_api_v1_models_secret_proto_msgTypes,
	}.Build()
	File_infra_unifiedfleet_api_v1_models_secret_proto = out.File
	file_infra_unifiedfleet_api_v1_models_secret_proto_rawDesc = nil
	file_infra_unifiedfleet_api_v1_models_secret_proto_goTypes = nil
	file_infra_unifiedfleet_api_v1_models_secret_proto_depIdxs = nil
}
