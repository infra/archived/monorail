// Copyright 2023 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package `coverage` contains all the necessary code to serve the
// new code coverage dashboard for chromium/src.
package coverage
