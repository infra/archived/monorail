// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package clients

//go:generate mockgen -source swarming.go -destination swarming.mock.go -package clients -write_package_comment=false -copyright_file=copyright
