// Copyright 2019 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import (
	"google.golang.org/appengine/v2"
)

func main() {
	appengine.Main()
}
