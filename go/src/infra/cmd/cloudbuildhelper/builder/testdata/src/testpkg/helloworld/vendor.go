// Copyright 2020 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package main

import "example.com/pkg"

// C exists to make golint happy.
const C = pkg.A
