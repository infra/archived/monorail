// Code generated by cproto. DO NOT EDIT.

package fleetconsolerpc

import "go.chromium.org/luci/grpc/discovery"

import "google.golang.org/protobuf/types/descriptorpb"

func init() {
	discovery.RegisterDescriptorSetCompressed(
		[]string{
			"fleetconsole.FleetConsole",
		},
		[]byte{31, 139,
			8, 0, 0, 0, 0, 0, 0, 255, 148, 148, 209, 110, 243, 52,
			20, 199, 227, 56, 68, 249, 206, 2, 3, 111, 176, 125, 25, 210,
			206, 118, 1, 87, 107, 167, 50, 33, 216, 196, 5, 235, 54, 52,
			196, 16, 98, 131, 139, 221, 165, 233, 73, 98, 72, 237, 98, 59,
			147, 250, 8, 220, 243, 38, 220, 242, 22, 188, 16, 114, 179, 78,
			29, 173, 244, 109, 119, 254, 215, 199, 231, 255, 243, 223, 167, 129,
			127, 223, 192, 137, 84, 165, 201, 251, 101, 67, 228, 10, 173, 172,
			110, 168, 159, 79, 229, 179, 31, 204, 180, 232, 91, 50, 15, 178,
			160, 222, 212, 104, 167, 69, 186, 188, 125, 248, 62, 108, 252, 36,
			85, 245, 51, 253, 209, 146, 117, 135, 31, 64, 218, 73, 59, 213,
			202, 210, 97, 6, 187, 94, 95, 144, 111, 112, 147, 171, 188, 34,
			179, 168, 221, 131, 183, 107, 246, 186, 131, 131, 191, 25, 164, 87,
			222, 104, 216, 25, 137, 111, 32, 242, 213, 226, 109, 111, 217, 191,
			183, 100, 158, 101, 235, 182, 186, 126, 98, 12, 31, 173, 152, 137,
			207, 86, 15, 172, 35, 205, 62, 127, 103, 93, 231, 114, 254, 213,
			253, 151, 47, 203, 244, 236, 127, 250, 251, 127, 98, 136, 69, 20,
			5, 200, 224, 47, 6, 44, 21, 60, 10, 196, 224, 79, 134, 67,
			61, 157, 25, 89, 213, 14, 7, 199, 131, 19, 188, 171, 9, 135,
			181, 209, 19, 217, 78, 240, 219, 214, 213, 218, 88, 192, 95, 44,
			161, 46, 209, 213, 210, 162, 213, 173, 41, 8, 11, 61, 38, 148,
			22, 43, 253, 64, 70, 209, 24, 71, 51, 204, 241, 252, 246, 226,
			200, 186, 89, 67, 216, 200, 130, 148, 37, 116, 117, 238, 176, 200,
			21, 142, 8, 176, 212, 173, 26, 163, 84, 232, 106, 194, 31, 174,
			135, 151, 63, 222, 94, 98, 41, 27, 234, 1, 36, 192, 66, 193,
			227, 224, 99, 191, 74, 4, 79, 130, 27, 120, 3, 97, 178, 209,
			45, 1, 194, 56, 16, 17, 4, 31, 50, 0, 224, 113, 192, 4,
			135, 100, 27, 42, 136, 226, 32, 12, 4, 79, 195, 126, 118, 143,
			62, 59, 28, 107, 178, 168, 180, 67, 75, 106, 140, 218, 160, 161,
			130, 228, 3, 97, 174, 102, 40, 85, 169, 205, 36, 119, 82, 171,
			30, 94, 59, 252, 173, 181, 14, 139, 154, 138, 223, 109, 7, 235,
			217, 30, 71, 210, 95, 208, 213, 100, 60, 95, 10, 239, 121, 35,
			38, 120, 26, 63, 169, 80, 240, 116, 227, 147, 133, 226, 130, 167,
			7, 71, 160, 231, 72, 76, 240, 205, 240, 215, 108, 132, 43, 207,
			137, 185, 115, 52, 153, 58, 139, 78, 99, 161, 149, 203, 11, 135,
			207, 43, 92, 109, 116, 91, 213, 56, 31, 81, 124, 156, 209, 158,
			127, 135, 178, 109, 176, 212, 6, 199, 52, 106, 171, 74, 170, 234,
			9, 141, 121, 199, 120, 103, 161, 66, 193, 55, 119, 143, 23, 138,
			11, 190, 121, 118, 7, 95, 67, 24, 5, 34, 218, 10, 182, 89,
			118, 132, 75, 195, 141, 82, 57, 82, 62, 148, 188, 105, 102, 29,
			149, 84, 243, 20, 235, 206, 4, 128, 71, 254, 246, 91, 201, 22,
			156, 66, 24, 49, 17, 237, 4, 187, 44, 235, 225, 242, 31, 225,
			101, 125, 60, 234, 78, 178, 13, 87, 16, 70, 161, 136, 246, 130,
			79, 89, 118, 186, 26, 212, 171, 216, 124, 224, 123, 9, 194, 119,
			16, 70, 92, 68, 251, 1, 178, 236, 108, 93, 207, 215, 128, 114,
			38, 248, 126, 114, 48, 138, 231, 223, 166, 47, 254, 11, 0, 0,
			255, 255, 128, 58, 44, 144, 214, 4, 0, 0},
	)
}

// FileDescriptorSet returns a descriptor set for this proto package, which
// includes all defined services, and all transitive dependencies.
//
// Will not return nil.
//
// Do NOT modify the returned descriptor.
func FileDescriptorSet() *descriptorpb.FileDescriptorSet {
	// We just need ONE of the service names to look up the FileDescriptorSet.
	ret, err := discovery.GetDescriptorSet("fleetconsole.FleetConsole")
	if err != nil {
		panic(err)
	}
	return ret
}
