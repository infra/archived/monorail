// Copyright 2024 The Chromium Authors
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Package scheduling implements a library for task scheduling. It is used to
// schedule tasks using different task scheduling providers.
package scheduling
