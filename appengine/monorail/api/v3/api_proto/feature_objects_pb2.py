# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api/v3/api_proto/feature_objects.proto
"""Generated protocol buffer code."""
from google.protobuf.internal import builder as _builder
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from google.api import field_behavior_pb2 as google_dot_api_dot_field__behavior__pb2
from google.api import resource_pb2 as google_dot_api_dot_resource__pb2
from google.protobuf import timestamp_pb2 as google_dot_protobuf_dot_timestamp__pb2
from api.v3.api_proto import issue_objects_pb2 as api_dot_v3_dot_api__proto_dot_issue__objects__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n&api/v3/api_proto/feature_objects.proto\x12\x0bmonorail.v3\x1a\x1fgoogle/api/field_behavior.proto\x1a\x19google/api/resource.proto\x1a\x1fgoogle/protobuf/timestamp.proto\x1a$api/v3/api_proto/issue_objects.proto\"\xac\x03\n\x07Hotlist\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x19\n\x0c\x64isplay_name\x18\x02 \x01(\tB\x03\xe0\x41\x02\x12)\n\x05owner\x18\x03 \x01(\tB\x1a\xfa\x41\x14\n\x12\x61pi.crbug.com/User\xe0\x41\x02\x12(\n\x07\x65\x64itors\x18\x04 \x03(\tB\x17\xfa\x41\x14\n\x12\x61pi.crbug.com/User\x12\x14\n\x07summary\x18\x05 \x01(\tB\x03\xe0\x41\x02\x12\x18\n\x0b\x64\x65scription\x18\x06 \x01(\tB\x03\xe0\x41\x02\x12\x36\n\x0f\x64\x65\x66\x61ult_columns\x18\x07 \x03(\x0b\x32\x1d.monorail.v3.IssuesListColumn\x12<\n\x0fhotlist_privacy\x18\x08 \x01(\x0e\x32#.monorail.v3.Hotlist.HotlistPrivacy\"J\n\x0eHotlistPrivacy\x12\x1f\n\x1bHOTLIST_PRIVACY_UNSPECIFIED\x10\x00\x12\x0b\n\x07PRIVATE\x10\x01\x12\n\n\x06PUBLIC\x10\x02:1\xea\x41.\n\x15\x61pi.crbug.com/Hotlist\x12\x15hotlists/{hotlist_id}\"\x90\x02\n\x0bHotlistItem\x12\x0c\n\x04name\x18\x01 \x01(\t\x12*\n\x05issue\x18\x02 \x01(\tB\x1b\xfa\x41\x15\n\x13\x61pi.crbug.com/Issue\xe0\x41\x05\x12\x11\n\x04rank\x18\x03 \x01(\rB\x03\xe0\x41\x03\x12)\n\x05\x61\x64\x64\x65r\x18\x04 \x01(\tB\x1a\xfa\x41\x14\n\x12\x61pi.crbug.com/User\xe0\x41\x03\x12\x34\n\x0b\x63reate_time\x18\x05 \x01(\x0b\x32\x1a.google.protobuf.TimestampB\x03\xe0\x41\x03\x12\x0c\n\x04note\x18\x06 \x01(\t:E\xea\x41\x42\n\x19\x61pi.crbug.com/HotlistItem\x12%hotlists/{hotlist_id}/items/{item_id}B#Z!infra/monorailv2/api/v3/api_protob\x06proto3')

_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, globals())
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'api.v3.api_proto.feature_objects_pb2', globals())
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  DESCRIPTOR._serialized_options = b'Z!infra/monorailv2/api/v3/api_proto'
  _HOTLIST.fields_by_name['display_name']._options = None
  _HOTLIST.fields_by_name['display_name']._serialized_options = b'\340A\002'
  _HOTLIST.fields_by_name['owner']._options = None
  _HOTLIST.fields_by_name['owner']._serialized_options = b'\372A\024\n\022api.crbug.com/User\340A\002'
  _HOTLIST.fields_by_name['editors']._options = None
  _HOTLIST.fields_by_name['editors']._serialized_options = b'\372A\024\n\022api.crbug.com/User'
  _HOTLIST.fields_by_name['summary']._options = None
  _HOTLIST.fields_by_name['summary']._serialized_options = b'\340A\002'
  _HOTLIST.fields_by_name['description']._options = None
  _HOTLIST.fields_by_name['description']._serialized_options = b'\340A\002'
  _HOTLIST._options = None
  _HOTLIST._serialized_options = b'\352A.\n\025api.crbug.com/Hotlist\022\025hotlists/{hotlist_id}'
  _HOTLISTITEM.fields_by_name['issue']._options = None
  _HOTLISTITEM.fields_by_name['issue']._serialized_options = b'\372A\025\n\023api.crbug.com/Issue\340A\005'
  _HOTLISTITEM.fields_by_name['rank']._options = None
  _HOTLISTITEM.fields_by_name['rank']._serialized_options = b'\340A\003'
  _HOTLISTITEM.fields_by_name['adder']._options = None
  _HOTLISTITEM.fields_by_name['adder']._serialized_options = b'\372A\024\n\022api.crbug.com/User\340A\003'
  _HOTLISTITEM.fields_by_name['create_time']._options = None
  _HOTLISTITEM.fields_by_name['create_time']._serialized_options = b'\340A\003'
  _HOTLISTITEM._options = None
  _HOTLISTITEM._serialized_options = b'\352AB\n\031api.crbug.com/HotlistItem\022%hotlists/{hotlist_id}/items/{item_id}'
  _HOTLIST._serialized_start=187
  _HOTLIST._serialized_end=615
  _HOTLIST_HOTLISTPRIVACY._serialized_start=490
  _HOTLIST_HOTLISTPRIVACY._serialized_end=564
  _HOTLISTITEM._serialized_start=618
  _HOTLISTITEM._serialized_end=890
# @@protoc_insertion_point(module_scope)
